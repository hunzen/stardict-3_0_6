/*
 * Copyright 2023 Hu Zheng <huzheng001@gmail.com>
 *
 * This file is part of StarDict.
 *
 * StarDict is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StarDict is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StarDict.  If not, see <http://www.gnu.org/licenses/>.
 */

// Notice: this dict.cn plugin is outdated, as dict.cn website closed the xml querying interface, but the html querying interface still works.

#include "stardict_dictdotcn_xml.h"
#include <glib/gi18n.h>
#include <cstring>
#include <string>
#include <list>

#ifdef _WIN32
#include <windows.h>
#endif

#define DICTDOTCN "apii.dict.cn"

static const StarDictPluginSystemInfo *plugin_info = NULL;
static const StarDictPluginSystemService *plugin_service;
static IAppDirs* gpAppDirs = NULL;

struct QueryInfo {
	bool ismainwin;
	char *word;
};

static std::list<QueryInfo *> keyword_list;

struct dict_ParseUserData {
	std::string pron;
	std::string def;
	std::string rel;
	std::list<std::pair<std::string, std::string> > sentences;
	std::string orig;
	std::string trans;
	std::list<std::string> suggestions;
};

static void dict_parse_text(GMarkupParseContext *context, const gchar *text, gsize text_len, gpointer user_data, GError **error)
{
	const gchar *element = g_markup_parse_context_get_element(context);
	if (!element)
		return;
	dict_ParseUserData *Data = (dict_ParseUserData *)user_data;
	if (strcmp(element, "pron")==0) {
		Data->pron.assign(text, text_len);
	} else if (strcmp(element, "def")==0) {
		Data->def.assign(text, text_len);
	} else if (strcmp(element, "rel")==0) {
		Data->rel.assign(text, text_len);
	} else if (strcmp(element, "orig")==0) {
		Data->orig.assign(text, text_len);
	} else if (strcmp(element, "trans")==0) {
		Data->trans.assign(text, text_len);
	} else if (strcmp(element, "sugg")==0) {
		Data->suggestions.push_back(std::string(text, text_len));
	}
}

static void dict_parse_start_element(GMarkupParseContext *context, const gchar *element_name, const gchar **attribute_names, const gchar **attribute_values, gpointer user_data, GError **error)
{
	if (strcmp(element_name, "sent")==0) {
		dict_ParseUserData *Data = (dict_ParseUserData *)user_data;
		Data->orig.clear();
		Data->trans.clear();
	}
}

static void dict_parse_end_element(GMarkupParseContext *context, const gchar *element_name, gpointer user_data, GError **error)
{
	if (strcmp(element_name, "sent")==0) {
		dict_ParseUserData *Data = (dict_ParseUserData *)user_data;
		Data->sentences.push_back(std::pair<std::string, std::string>(Data->orig, Data->trans));
	}
}

static void process_xml_response(const char *data, size_t data_len, NetDictResponse *resp)
{
	resp->data = NULL;
	gchar *content = NULL;
	// search for xml processing instruction, it may specify custom encoding
	do {
		const char *pi = g_strstr_len(data, data_len, "<?xml");
		if(!pi)
			break;
		const char *pi_end = g_strstr_len(pi, data_len - (pi - data), "?>");
		if(!pi_end)
			break;
		const char *enc = g_strstr_len(pi, pi_end - pi, "encoding=");
		if(!enc)
			break;
		enc += sizeof("encoding=") - 1;
		char quote = *enc;
		if(quote != '\'' && quote != '"')
			break;
		enc += 1;
		const char* enc_end = strchr(enc, quote);
		if(!enc_end)
			break;
		std::string encoding(enc, enc_end - enc);
		if (encoding.empty())
			break;
		gsize bytes_written;
		content = g_convert(data, data_len, "UTF-8", encoding.c_str(), NULL, &bytes_written, NULL);
		data = content;
		data_len = bytes_written;
	} while(false);
	if((!data) || (data_len ==0))
		return;
	const char *xml = g_strstr_len(data, data_len, "<dict>");
	if (!xml) {
	    std::string definition(data, data_len);
	    resp->data = plugin_service->build_dictdata('m', definition.c_str());
	    g_free(content);
		return;
    }
	const char *xml_end = g_strstr_len(xml+6, data_len - (xml+6 - data), "</dict>");
	if (!xml_end) {
		g_free(content);
		return;
	}
	xml_end += 7;
	dict_ParseUserData Data;
	GMarkupParser parser;
	parser.start_element = dict_parse_start_element;
	parser.end_element = dict_parse_end_element;
	parser.text = dict_parse_text;
	parser.passthrough = NULL;
	parser.error = NULL;
	GError *err = NULL;
	GMarkupParseContext* context = g_markup_parse_context_new(&parser, (GMarkupParseFlags)0, &Data, NULL);
	if(!g_markup_parse_context_parse(context, xml, xml_end - xml, &err)) {
		g_warning(_("Dict.cn xml plugin: context parse failed: %s"), err ? err->message : "");
		g_error_free(err);
		g_markup_parse_context_free(context);
		g_free(content);
		return;
	}
	if(!g_markup_parse_context_end_parse(context, &err)) {
		g_warning(_("Dict.cn xml plugin: context parse failed: %s"), err ? err->message : "");
		g_error_free(err);
		g_markup_parse_context_free(context);
		g_free(content);
		return;
	}
	g_markup_parse_context_free(context);
	//context = NULL;
	if ((Data.def.empty() || Data.def == "Not Found") && Data.suggestions.empty()) {
		g_free(content);
		return;
	}

	std::string definition;
	if (!Data.pron.empty()) {
		definition += "[";
		definition += Data.pron;
		definition += "]";
	}
	if(!Data.def.empty()) {
		if(!definition.empty())
			definition += "\n";
		definition += Data.def;
	}
	if (!Data.rel.empty()) {
		if(!definition.empty())
			definition += "\n";
		definition += Data.rel;
	}
	if (!Data.sentences.empty()) {
		if(!definition.empty())
			definition += "\n\n";
		definition += "例句与用法：";
		int index = 1;
		char *tmp_str;
		for (std::list<std::pair<std::string, std::string> >::iterator i = Data.sentences.begin(); i != Data.sentences.end(); ++i) {
			tmp_str = g_strdup_printf("\n%d. %s\n   %s", index, i->first.c_str(), i->second.c_str());
			definition += tmp_str;
			g_free(tmp_str);
			index++;
		}
	}
	if (!Data.suggestions.empty()) {
		if(!definition.empty())
			definition += "\n\n";
		definition += "建议单词：";
		for(std::list<std::string>::const_iterator it=Data.suggestions.begin(); it != Data.suggestions.end(); ++it) {
			definition += "\n";
			definition += *it;
		}
	}
	resp->data = plugin_service->build_dictdata('m', definition.c_str());
	g_free(content);
}

static void on_get_http_response(const char *buffer, size_t buffer_len, gpointer userdata)
{
	if (!buffer) {
		return;
	}
	const char *p = g_strstr_len(buffer, buffer_len, "\r\n\r\n");
	if (!p) {
		return;
	}
	p += 4;
	QueryInfo *qi = (QueryInfo *)userdata;
	NetDictResponse *resp = new NetDictResponse;
	resp->bookname = _("Dict.cn xml");
	resp->booklink = "http://www.dict.cn";
	resp->word = qi->word; // So neen't free qi->word;
	process_xml_response(p, buffer_len - (p - buffer), resp);
	plugin_service->netdict_save_cache_resp(DICTDOTCN, qi->word, resp);
	plugin_service->show_netdict_resp(DICTDOTCN, resp, qi->ismainwin);
	delete qi;
	keyword_list.remove(qi);
}

static void lookup(const char *word, bool ismainwin)
{
	std::string file = "/ws.php?utf8=true&q=";
	char *eword = plugin_service->encode_uri_string(word);
	file += eword;
	g_free(eword);
	gchar *keyword = g_strdup(word);
	QueryInfo *qi = new QueryInfo;
	qi->ismainwin = ismainwin;
	qi->word = keyword;
	keyword_list.push_back(qi);
	plugin_service->send_http_request("apii.dict.cn", file.c_str(), on_get_http_response, qi);
}

DLLIMPORT bool stardict_plugin_init(StarDictPlugInObject *obj, IAppDirs* appDirs)
{
	g_debug(_("Loading Dict.cn xml plug-in..."));
	if (strcmp(obj->version_str, PLUGIN_SYSTEM_VERSION)!=0) {
		g_print(_("Error: Dict.cn xml plugin version doesn't match!\n"));
		return true;
	}
	obj->type = StarDictPlugInType_NETDICT;
	obj->info_xml = g_strdup_printf("<plugin_info><name>%s</name><version>1.0</version><short_desc>%s</short_desc><long_desc>%s</long_desc><author>Hu Zheng &lt;huzheng001@gmail.com&gt;</author><website>http://stardict-4.sourceforge.net</website></plugin_info>", _("Dict.cn xml"), _("Dict.cn network xml dictionary."), _("Query result from Dict.cn xml website."));
	obj->configure_func = NULL;
	plugin_info = obj->plugin_info;
	plugin_service = obj->plugin_service;
	gpAppDirs = appDirs;
	return false;
}

DLLIMPORT void stardict_plugin_exit(void)
{
	for (std::list<QueryInfo *>::iterator i = keyword_list.begin(); i != keyword_list.end(); ++i) {
		g_free((*i)->word);
		delete *i;
	}
	gpAppDirs = NULL;
}

DLLIMPORT bool stardict_netdict_plugin_init(StarDictNetDictPlugInObject *obj)
{
	obj->lookup_func = lookup;
	obj->dict_name = _("Dict.cn xml");
	obj->author = _("Hu Zheng");
	obj->email = _("huzheng001@gmail.com");
	obj->website = _("http://www.dict.cn");
	obj->date = _("2023.12.22");
	obj->dict_link = "http://www.dict.cn";
	obj->dict_cacheid = DICTDOTCN;
	g_print(_("Dict.cn network dictionary xml plug-in \033[31m[loaded]\033[0m.\n"));
	return false;
}

#ifdef _WIN32
BOOL APIENTRY DllMain (HINSTANCE hInst     /* Library instance handle. */ ,
                       DWORD reason        /* Reason this function is being called. */ ,
                       LPVOID reserved     /* Not used. */ )
{
    switch (reason)
    {
      case DLL_PROCESS_ATTACH:
        break;

      case DLL_PROCESS_DETACH:
        break;

      case DLL_THREAD_ATTACH:
        break;

      case DLL_THREAD_DETACH:
        break;
    }

    /* Returns TRUE on success, FALSE on failure */
    return TRUE;
}
#endif
